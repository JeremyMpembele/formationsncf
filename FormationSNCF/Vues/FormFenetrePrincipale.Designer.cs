﻿namespace FormationSNCF.Vues
{
    partial class FormFenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuPrincipalMenuStrip = new System.Windows.Forms.MenuStrip();
            this.lieuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesLieuxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aGENTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gESTIONAGENTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fORMATIONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aCTIVITEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lISTEDESAGENTSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPrincipalMenuStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPrincipalMenuStrip
            // 
            this.menuPrincipalMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lieuToolStripMenuItem,
            this.aGENTToolStripMenuItem,
            this.fORMATIONToolStripMenuItem});
            this.menuPrincipalMenuStrip.Location = new System.Drawing.Point(0, 24);
            this.menuPrincipalMenuStrip.Name = "menuPrincipalMenuStrip";
            this.menuPrincipalMenuStrip.Size = new System.Drawing.Size(284, 24);
            this.menuPrincipalMenuStrip.TabIndex = 1;
            this.menuPrincipalMenuStrip.Text = "menuStrip1";
            this.menuPrincipalMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // lieuToolStripMenuItem
            // 
            this.lieuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesLieuxToolStripMenuItem});
            this.lieuToolStripMenuItem.Name = "lieuToolStripMenuItem";
            this.lieuToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.lieuToolStripMenuItem.Text = "LIEUX";
            this.lieuToolStripMenuItem.Click += new System.EventHandler(this.GestionDesLieuxToolStripMenuItem_Click);
            // 
            // gestionDesLieuxToolStripMenuItem
            // 
            this.gestionDesLieuxToolStripMenuItem.Name = "gestionDesLieuxToolStripMenuItem";
            this.gestionDesLieuxToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.gestionDesLieuxToolStripMenuItem.Text = "GESTION DES LIEUX";
            this.gestionDesLieuxToolStripMenuItem.Click += new System.EventHandler(this.GestionDesLieuxToolStripmenuItem_Click);
            // 
            // aGENTToolStripMenuItem
            // 
            this.aGENTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gESTIONAGENTToolStripMenuItem,
            this.lISTEDESAGENTSToolStripMenuItem});
            this.aGENTToolStripMenuItem.Name = "aGENTToolStripMenuItem";
            this.aGENTToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.aGENTToolStripMenuItem.Text = "AGENT";
            this.aGENTToolStripMenuItem.Click += new System.EventHandler(this.aGENTToolStripMenuItem_Click);
            // 
            // gESTIONAGENTToolStripMenuItem
            // 
            this.gESTIONAGENTToolStripMenuItem.Name = "gESTIONAGENTToolStripMenuItem";
            this.gESTIONAGENTToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.gESTIONAGENTToolStripMenuItem.Text = "GESTION AGENT";
            this.gESTIONAGENTToolStripMenuItem.Click += new System.EventHandler(this.AjouterUnAgentToolStripMenuItem_Click);
            // 
            // fORMATIONToolStripMenuItem
            // 
            this.fORMATIONToolStripMenuItem.Name = "fORMATIONToolStripMenuItem";
            this.fORMATIONToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.fORMATIONToolStripMenuItem.Text = "FORMATION";
            this.fORMATIONToolStripMenuItem.Click += new System.EventHandler(this.FormationToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aCTIVITEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aCTIVITEToolStripMenuItem
            // 
            this.aCTIVITEToolStripMenuItem.Name = "aCTIVITEToolStripMenuItem";
            this.aCTIVITEToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.aCTIVITEToolStripMenuItem.Text = "ACTIVITE";
            this.aCTIVITEToolStripMenuItem.Click += new System.EventHandler(this.ActiviteToolStripMenuItem_Click);
            // 
            // lISTEDESAGENTSToolStripMenuItem
            // 
            this.lISTEDESAGENTSToolStripMenuItem.Name = "lISTEDESAGENTSToolStripMenuItem";
            this.lISTEDESAGENTSToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.lISTEDESAGENTSToolStripMenuItem.Text = "LISTE DES AGENTS";
            this.lISTEDESAGENTSToolStripMenuItem.Click += new System.EventHandler(this.lISTEDESAGENTSToolStripMenuItem_Click);
            // 
            // FormFenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = global::FormationSNCF.Properties.Resources.logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.menuPrincipalMenuStrip);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuPrincipalMenuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFenetrePrincipale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormationSNCF";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFenetrePrincipale_FormClosing);
            this.Load += new System.EventHandler(this.FormFenetrePrincipale_Load);
            this.menuPrincipalMenuStrip.ResumeLayout(false);
            this.menuPrincipalMenuStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipalMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem lieuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesLieuxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aGENTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gESTIONAGENTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fORMATIONToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aCTIVITEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lISTEDESAGENTSToolStripMenuItem;
    }
}