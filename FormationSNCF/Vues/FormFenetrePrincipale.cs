﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormFenetrePrincipale : Form
    {

        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set 
            { 
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }


        public FormFenetrePrincipale()
        {
            InitializeComponent();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void GestionDesLieuxToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FormFenetrePrincipale_Load(object sender, EventArgs e)
        {

        }

        private void FormFenetrePrincipaleForm_Closing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void GestionDesLieuxToolStripmenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionLieu();
        }

        private void AjouterUnAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormAjouterAgent();
        }

        private void GestionDesActionsDeFormationsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FormationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionActionFormation();
        }

        private void FormFenetrePrincipale_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }

        private void ActiviteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionActivite();
        }

        private void aGENTToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void lISTEDESAGENTSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormListeAgent();
        }
    }
}
