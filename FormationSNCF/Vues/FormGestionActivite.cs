﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionActivite : Form
    {
        public FormGestionActivite()
        {
            InitializeComponent();
        }

        private void buttonAjoutActivite_Click(object sender, EventArgs e)
        {
            if (textBoxNomActivite.Text.Length < 3)
            {
                MessageBox.Show("L'activité doit être composée d'au moins 3 caractères");
                textBoxNomActivite.Text = "";
                textBoxNomActivite.Focus();
            }
            else
            {
                
                bool trouve = false;
                foreach (Activite activiteCourante in Donnees.CollectionActivite)
                {
                    if (activiteCourante.LibelleActivite == textBoxNomActivite.Text)
                    {
                        trouve = true;
                        break;
                    }
                }
                if (trouve == false)
                {
                    Activite uneActivite = new Activite(textBoxNomActivite.Text);
                    Donnees.CollectionActivite.Add(uneActivite);
                    listBoxListeActivites.DataSource = null;
                    listBoxListeActivites.DataSource = Donnees.CollectionActivite;
                }
            }  
        }

        private void FormGestionActivite_Load_1(object sender, EventArgs e)
        {
            listBoxListeActivites.DataSource = Donnees.CollectionActivite;
        }
    }
}
